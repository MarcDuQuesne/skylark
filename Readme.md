### Docker

- To build the image, run:
```bash
docker build -t "skylark:latest" .
```

- To run the image, run then:
```bash
docker run -p 8050:5000 skylark:latest
```

- To run interactively, run:
```bash
docker run --rm -it skylark:dockerfile bash
```

### TODO

##### leaflet

use [leaflet on a custom map](https://leafletjs.com/examples/crs-simple/crs-simple.html)

##### worse alternatives:
[overlay](https://plot.ly/python/images/) graph to a image:

[superpose](https://stackoverflow.com/questions/18339549/floating-div-over-an-image/18339713) a div to an image:

##### flask frontend

https://realpython.com/the-ultimate-flask-front-end-part-2/

##### bootstrap
https://pythonhosted.org/Flask-Bootstrap/basic-usage.html
https://github.com/mbr/flask-bootstrap/tree/master/sample_app
https://startbootstrap.com/template-overviews/sb-admin/
https://startbootstrap.com/template-categories/all/
