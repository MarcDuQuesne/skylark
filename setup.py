from setuptools import setup

setup(
    name='skylark',
    packages=['skylark'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)