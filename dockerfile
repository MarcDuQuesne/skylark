FROM python:3.6
MAINTAINER Matteo Giani "matteo.giani.87@gmail.com"

COPY . /app
WORKDIR /app

RUN pip install --upgrade pip
RUN pip install -r /app/skylark/requirements.txt
RUN pip install /app

EXPOSE 5000
ENV FLASK_DEBUG=true
ENV FLASK_APP=skylark
RUN flask init-db
CMD flask run --host=0.0.0.0
