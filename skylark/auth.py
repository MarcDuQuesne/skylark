import functools

from flask import Blueprint, g, redirect, url_for, request, render_template

auth = Blueprint('auth', __name__, url_prefix='/auth')

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if 'user' not in g or g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view

@auth.route('/login', methods=['GET', 'POST'])
def login():
    """Log in a registered user by adding the user id to the session."""
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'

        if error is None:
            # store the user id in a new session and return to the index
            # flash('You were logged in')
            session.clear()
            session['user_id'] = user['id']
            return redirect(url_for('index'))

        flash(error)

    return render_template('sb-admin/login.html', error=error)

@auth.route('/logout')
def logout():
    session.clear()
    flash('You were logged out')
    return redirect(url_for('index'))