# all the imports
import os
import sqlite3

from flask import render_template

from skylark.factory import create_app
from skylark.auth import auth, login_required

app = create_app()
app.register_blueprint(auth)

@app.route('/')
def index():
    """Initial page"""
    return render_template('sb-admin/index.html')

@app.route('/hello')
def hello():
    """Test page"""
    return render_template('sb-admin/index.html')

#def run():
    #http_server = WSGIServer(('', config.PORT), app)
    #http_server.serve_forever()
#    app.run(port=config.PORT)

#if __name__ == '__main__':
#    run()