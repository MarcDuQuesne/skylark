import os

from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash

def create_app(test_config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__,instance_relative_config=True)
    app.config.from_mapping(
        # a default secret that should be overridden by instance config
        SECRET_KEY='dev',
        # store the database in the instance folder
        DATABASE=os.path.join(app.root_path, 'skylark.db')
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        # app.config.from_pyfile('config.py', silent=True)
        # Load default config and override config from an environment variable
        app.config.update(dict(
            SECRET_KEY='dev',
            USERNAME='admin',
            PASSWORD='default'
        ))
        app.config.from_envvar('SKYLARK_SETTINGS', silent=True)
    else:
        # load the test config if passed in
        app.config.update(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # register the database commands
    # from flaskr import db
    # db.init_app(app)

    app.add_url_rule('/', endpoint='hello')

    return app